
import React, { useState, useContext, useEffect, FC } from 'react';
import Context from '../context/context';
import {StyleSheet, Text, View, Image, TouchableOpacity, Alert, ScrollView} from 'react-native';
import httpService from '../services/http.service';


export const InvitesList:FC = ({navigation}:any) => {
  const [inviteList, setInviteList] = useState([]);
  const context = useContext(Context);

  useEffect(() => {
    (async () => {
      const allInvites = await context.userState.invites;
      setInviteList(allInvites);
    })()
  },[])


  return(
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.scroll}>
          {
            inviteList.map((user,index)=>(
              <View key={user.id + index} style={styles.user}>
                <Image source={{uri: user.avatar}} style={styles.image} />
                <Text style={styles.text}>{user.firstName} {user.lastName}</Text>
                <View style={styles.addButton}>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => {}}
                    >
                      <Text style={styles.buttonText}>Accept</Text>
                  </TouchableOpacity>
                </View>
              </View>
            ))
          }
        </View>
      </ScrollView>
    </View> 
  )
}

const styles = StyleSheet.create({
  scroll: {
    marginBottom: 85
  },
  addButton:{
    alignContent: 'stretch'
  },
  user:{
    flexDirection: 'row',
    height: 70,
    alignItems: 'center',
    textAlign: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  image:{
    height: 50,
    width: 50,
    margin: 5
  },
  container: {
    flex: 5,
    fontSize: 36,
    backgroundColor: '#ffd300',
    justifyContent: 'center',
  },
  text: {
    fontSize: 22,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    color: 'black'
  },
  button:{
    alignItems: "center",
    alignSelf: 'flex-end',
    backgroundColor: "black",
    padding: 7,
    width: 130,
    borderRadius: 10
  },
  buttonText:{
      color: 'white',
      fontSize: 16
  },
})