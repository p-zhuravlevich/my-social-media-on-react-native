import {View, TextInput, TouchableOpacity, StyleSheet, Text, Alert} from 'react-native';
import React, {useState, useContext, useEffect, FC} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Context from "../context/context";
import httpService from '../services/http.service';


const Login: FC = ({navigation}:any) =>{
    const [email, setEmail] = useState<any>('');
    const [password, setPassword] = useState<any>('');
    const context = useContext(Context);

    const logIn = async() =>{
        const allFields = await httpService.signIn({email, password});
        if(allFields){
          context.handleShowUser(allFields);
          navigation.navigate('Feed');
          setEmail("");
          setPassword("");
        }
      }
    

    return (
        <View style={styles.container}>
        <Text style={styles.text}>Please, sign in for use this great social media:</Text>
          <View>
            <Text style={{color: 'black'}}>Email: </Text>
            <TextInput
              style={styles.input}
              onChangeText={setEmail}
              value={email}
              placeholder="Email"
            />
          </View>
          <View>
            <Text style={{color: 'black'}}>Password: </Text>
            <TextInput
              style={styles.input}
              onChangeText={setPassword}
              value={password}
              placeholder="Password"
            />
          </View>
          <View >
          <TouchableOpacity
            style={styles.button}
            onPress={() => {logIn()}}
            >
                <Text style={styles.buttonText}>Sign in</Text>
            </TouchableOpacity>
            </View>
          <View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Registration')}
            >
                <Text style={styles.buttonText}>Sign up</Text>
            </TouchableOpacity>
            </View>
    </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 5,
        fontSize: 36,
        backgroundColor: '#ffd300',
        alignItems: 'center',
        justifyContent: 'center',
      },
    text: {
        fontSize: 26,
        marginHorizontal: 55,
        marginBottom: 25,
        alignItems: 'center',
        justifyContent: 'center',
        color: 'black'
      },
    input: {
        height: 40,
        width: 300,
        marginBottom: 12,
        borderWidth: 1,
        padding: 10,
        backgroundColor: 'white',
        borderColor: 'rgb(153, 108, 48)'
      },
    button:{
        alignItems: "center",
        backgroundColor: "black",
        padding: 10,
        width: 100,
        margin: 15,
        borderRadius: 10
      },
      buttonText:{
          color: 'white',
          fontSize: 18
      }
})

export default Login;