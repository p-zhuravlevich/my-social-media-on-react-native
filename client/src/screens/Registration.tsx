import {View, TextInput, TouchableOpacity, StyleSheet, Text, Alert} from 'react-native';
import React, {useState, useContext, useEffect, FC} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import httpService from '../services/http.service';
import Context from "../context/context"

const Registration: FC = ({navigation}:any) => {
    const [data, setData] = useState<any>({
        email: '',
        password: '',
        firstName: '',
        lastName: ''
    });


    const handleInputChange = (name) => {
      return (value) => {
        const newData = {...data, [name]: value};
        setData(newData)
      }
    }

    const context = useContext(Context);
    
    const registration = async() =>{
      const allFields:any = await httpService.signUp(data);
      const email: any = allFields.email;
      const password:any = allFields.password;
      if(allFields){
        const body = await httpService.signIn({email, password});
        if(body){
          context.handleShowUser(body);
          setData({
            email: '',
            password: '',
            firstName: '',
            lastName: ''
          });
          navigation.navigate('Feed');
        }
      }
    }
    

    return (
        <View style={styles.container}>
        <Text style={styles.header}>Registration:</Text>
        <View>
            <Text style={styles.text}>First name: </Text>
            <TextInput
              style={styles.input}
              onChangeText={handleInputChange('firstName')}
              value={data.firstName}
              placeholder="First Name"
            />
          </View>
          <View>
            <Text style={styles.text}>Last name: </Text>
            <TextInput
              style={styles.input}
              onChangeText={handleInputChange('lastName')}
              value={data.lastName}
              placeholder="Last Name"
            />
          </View>
          <View>
            <Text style={styles.text}>Email: </Text>
            <TextInput
              style={styles.input}
              onChangeText={handleInputChange('email')}
              value={data.email}
              placeholder="Email"
            />
          </View>
          <View>
            <Text style={styles.text}>Password: </Text>
            <TextInput
              style={styles.input}
              onChangeText={handleInputChange('password')}
              value={data.password}
              placeholder="Password"
            />
          </View>
            <View >
          <TouchableOpacity
            style={styles.button}
            onPress={() => {registration()}}
            >
                <Text style={styles.buttonText}>Sign up</Text>
            </TouchableOpacity>
            </View>
    </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 5,
        fontSize: 36,
        backgroundColor: '#ffd300',
        alignItems: 'center',
        justifyContent: 'center',
      },
    text: {
        color: 'black'
      },
    header: {
        fontSize: 26,
        marginHorizontal: 55,
        marginBottom: 25,
        alignItems: 'center',
        justifyContent: 'center',
        color: 'black'
      },
    input: {
        height: 40,
        width: 300,
        marginBottom: 12,
        borderWidth: 1,
        padding: 10,
        backgroundColor: 'white',
        borderColor: 'rgb(153, 108, 48)'
      },
    button:{
        alignItems: "center",
        backgroundColor: "black",
        padding: 10,
        width: 100,
        margin: 15,
        borderRadius: 10
      },
      buttonText:{
          color: 'white',
          fontSize: 18
      }
})

export default Registration;