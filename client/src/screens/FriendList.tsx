import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect, FC } from 'react';
import Context from '../context/context'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TabNav from '../navigator/tabNavigator';
import httpService from '../services/http.service';


const FriendList:FC = ({navigation}:any) => {
  const context = useContext(Context);
  const [users, setUsers] = useState([]);
  const [invites, setInvite] = useState([]);
  console.log(context);



  useEffect(() => {
    (async () => {
      const allUsers = await httpService.getAllUsers()
      setUsers(allUsers);
    })()
  },[])

  const friendRequest = async(id:any) => {
    await httpService.sendInviteFriend(context.userState.id, id);
    const allUsers = await httpService.getAllUsers()
      setUsers(allUsers)
  }

  return(
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.scroll}>
          {
            users.map((user,index)=>(
              <View key={user.id + index} style={styles.user}>
                <Image source={{uri: user.avatar}} style={styles.image} />
                <Text style={styles.text}>{user.firstName} {user.lastName}</Text>
                <View style={styles.addButton}>
                {
                  (user.id === context.userState.id)
                  ?<TouchableOpacity
                    style={styles.buttonMy}
                    onPress={() => {navigation.navigate('Account')}}
                    >
                      <Text style={styles.buttonTextMy}>My profile</Text>
                  </TouchableOpacity>
                  :<TouchableOpacity
                    style={styles.button}
                    onPress={() => {friendRequest(user.id)}}
                    >
                      <Text style={styles.buttonText}>Add friend</Text>
                  </TouchableOpacity>
                }
                </View>
              </View>
            ))
          }
        </View>
      </ScrollView>
    </View> 
  )
}
export default FriendList;

const styles = StyleSheet.create({
  scroll: {
    marginBottom: 85
  },
  addButton:{
    alignContent: 'stretch'
  },
  user:{
    flexDirection: 'row',
    height: 70,
    alignItems: 'center',
    textAlign: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
  image:{
    height: 50,
    width: 50,
    margin: 5
  },
  container: {
    flex: 5,
    fontSize: 36,
    backgroundColor: '#ffd300',
    justifyContent: 'center',
  },
text: {
    fontSize: 22,
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    color: 'black'
  },
input: {
    height: 40,
    width: 300,
    marginBottom: 12,
    borderWidth: 1,
    padding: 10,
    backgroundColor: 'white',
    borderColor: 'rgb(153, 108, 48)'
  },
button:{
    alignItems: "center",
    alignSelf: 'flex-end',
    backgroundColor: "black",
    padding: 7,
    width: 130,
    borderRadius: 10
  },
  buttonText:{
      color: 'white',
      fontSize: 16
  },
  buttonTextMy:{
    color: 'black',
    fontSize: 16
  },  
  buttonMy:{
    alignItems: "center",
    alignSelf: 'flex-end',
    backgroundColor: "white",
    padding: 7,
    width: 130,
    borderRadius: 10,
    borderColor: 'black',
    borderWidth: 0.5
  },
})