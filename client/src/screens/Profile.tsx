import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState, useContext, useEffect, FC } from 'react';
import Context from '../context/context'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Profile: FC = ({navigation, route}:any) => {
  const [email, setEmail] = useState<string>("");
  const [firstName, setFirstName] = useState<string>("");
  const [lastName, setLastName] = useState<string>("");
  const context = useContext(Context);
  const [avatar, setAvatar] = useState<any>({uri:context.userState.avatar});

useEffect(() => {
  setEmail(context.userState.email)
  setFirstName(context.userState.firstName),  
  setLastName(context.userState.lastName),  
  setAvatar({uri:context.userState.avatar})  
}, []);

return (
  <View style={styles.container}>
    <Image style={styles.avatar} source={avatar} />
    <Text style={styles.text}>Your email: {email}</Text>
    <Text style={styles.text}>Your name: {firstName}</Text>
    <Text style={styles.text}>Your surname: {lastName}</Text>
        <View >
          <TouchableOpacity
            style={styles.button}
            onPress={() => {}}
            >
                <Text style={styles.buttonText}>Change user data</Text>
          </TouchableOpacity>
        </View>
  </View>
  );
}


const styles = StyleSheet.create({
container: {
  flex: 5,
  fontSize: 36,
  backgroundColor: '#ffd300',
  alignItems: 'center',
  justifyContent: 'center',
},
text: {
  margin: 15,
  fontSize: 26,
  color: 'black'
},
avatar:{
  width: 50,
  height: 50,
  margin: 5
},
button:{
  alignItems: "center",
  backgroundColor: "black",
  padding: 10,
  width: 190,
  margin: 15,
  borderRadius: 10
},
buttonText:{
    color: 'white',
    fontSize: 18
}
});

export default Profile;