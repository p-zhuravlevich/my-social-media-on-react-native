import {Alert} from 'react-native';
import {getTokenInfo, storeTokenInfo} from './asyncStorage.service';
import {iUserData} from '../context/context';
import {API_URL} from "@env";

interface iData {   
    email : string,
    password : string,
};

interface iNewPost {
    title : string,
    owner : string,
    text : string,
    picture?: string,
    video?: string
}

interface iUser {
    avatar: string,
    firstName: string,
    lastName: string,
}

class Http {
    private URL = 'http://192.168.31.176:6000';

    signUp = async(data : iData) : Promise < boolean | iUserData > => {
        try{
            const res = await fetch(`${this.URL}/register`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            if (res.status === 200) {
                const json = await res.json();
                Alert.alert('Message', 'User has been created!');
                return json
            } else {
                Alert.alert('Error', 'Something went wrong... Server is not answering!!!');
                return false;
            }
        }
        catch(e){
            console.log('error');
            console.log(e);
        }
    }

    signIn = async(data : iData) : Promise < boolean | iUserData > => {
        try{
            const res = await fetch(`${this.URL}/login`, {
                method: 'POST',
                body: JSON.stringify({
                    ...data
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            const json = await res.json();
            if (json.message) {
                Alert.alert('Error', json.message)
                return false;
            }

            storeTokenInfo({
                accessToken: json.accessToken,
                refreshToken: json.refreshToken,
                userId: json.userId
            });
            return json;
        }
        catch(e){
            console.log(e);
        }
    }

    getAllUsers = async () => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            console.log(tokenInfo);         
            if(tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${this.URL}/users/getallusers`, {
                    method: "GET",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });                  
                    const json = await res.json();
                    if(json.message) {
                        Alert.alert('Error', 'You have no authorization!!!')
                        return [];
                    }                  
                    return json;   
            }          
        } catch(e) {
            console.log(e);   
            return [];         
        }
    }

    sendInviteFriend = async (userId: string, newFriendId: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${this.URL}/users/sendInviteFriend`, {
                    method: "PUT",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    },
                    body: JSON.stringify({ userId, newFriendId })
                });
                const json = await res.json();
                if(json) {                                            
                    return json
                } 
                if(json.status === 'error') {
                    Alert.alert('Error', json.message)
                }                 
                return false;
                
            }
        } catch (e) {
            console.log(e);
        }
    }

    getAllInvites = async() => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const response = await fetch(`${this.URL}/users/invites`, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });
                const json = await response.json();
                if (response.status !== 200) {
                    return Alert.alert('Error', `${json}`);
                }
                return json;
            }
        } catch (e : Error | any) {
            Alert.alert('Error', `${e.message}`);
            return false
        }
    }
    
}
export default new Http();
