import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import Login from '../screens/Login'
import Registration from '../screens/Registration'
import DrawerNavigator from './drawerNavigator';

const screenOptionStyle = {
    headerStyle: {
      backgroundColor: "black",
    },
    headerTintColor: "white",
    headerBackTitle: "Back",
  };

  const Stack = createNativeStackNavigator();
  
  const MainStackNavigator = () => {
    return (
      <Stack.Navigator 
        initialRouteName="Login" 
        screenOptions={screenOptionStyle}> 
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Registration" component={Registration} />
            <Stack.Screen name="Feed" component={DrawerNavigator} options={{headerShown: false}}/>
      </Stack.Navigator>
  )
}

export { MainStackNavigator};
