import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Feather } from '@expo/vector-icons';

import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from '@react-navigation/native';
import TabNav from './tabNavigator';
import FriendList from '../screens/FriendList';

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "black",
  },
  headerTintColor: "white",
  headerBackTitle: "Back",
};

const Drawer = createDrawerNavigator();

export default function DrawerNavigator(){
  return(
      <Drawer.Navigator screenOptions={screenOptionStyle}>
        <Drawer.Screen name="Menu" component={TabNav} options={({ navigation })  => ({
          headerRight: () => (
            <TouchableOpacity onPress={() => (navigation.navigate('Login'))}>
                <Feather
                    name="power"
                    size={30}
                    color='#ffd300'
                    style={{
                    marginRight: 20
                }}/>
            </TouchableOpacity>
        )
        })}/>
        <Drawer.Screen name="Users" component={FriendList} options={({ navigation })  => ({
          headerRight: () => (
            <TouchableOpacity onPress={() => (navigation.navigate('Login'))}>
                <Feather
                    name="power"
                    size={30}
                    color='#ffd300'
                    style={{
                    marginRight: 20
                }}/>
            </TouchableOpacity>
        )
        })}/>
      </Drawer.Navigator>
  )
}
