import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Feed from '../screens/Feed';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Profile from '../screens/Profile';
import FriendList from '../screens/FriendList';
import { InvitesList } from '../screens/InvitesList';

const Tab = createBottomTabNavigator();

export default function TabNav() {
  return (
      <Tab.Navigator 
      
      screenOptions={({ route }) => ({
        tabBarShowLabel: false,
        tabBarStyle: {
          position: 'absolute',
          bottom: 10,
          left: 15,
          right: 15,
          elevation: 0,
          backgroundColor: '#d21900',
          borderRadius: 15,
          height: 70,
          
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 0.58,
            shadowRadius: 16.00,
          
        },
        tabBarIcon: ({ color, size }) => {
          let iconName;

          if (route.name === 'Account') {
            iconName = 'person';
            size = 30
          } else if (route.name === 'Feed') {
            iconName = 'today';
            size = 40
          }else if (route.name === 'Users1'){
            iconName = 'people';
            size = 40
          }else if (route.name === 'InviteFriends'){
            iconName = 'person-add';
            size = 30
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: 'black',
        tabBarInactiveTintColor: '#56501d',
      })}
      >
        <Tab.Screen name="Account" component={Profile} initialParams={{params: true}} options={{headerShown: false}} />
        <Tab.Screen name="Users1" component={FriendList} initialParams={{params: true}} options={{headerShown: false}} />
        <Tab.Screen name="InviteFriends" component={InvitesList} initialParams={{params: true}} options={{headerShown: false}} />
        <Tab.Screen name="Feed" component={Feed} initialParams={{params: true}} options={{headerShown: false}} />
        
      </Tab.Navigator>
  );
}
