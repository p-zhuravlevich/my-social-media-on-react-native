import {SHOW_USER, CHANGE_ACTIVE_USER_DATA} from "./types";

export const defaultState = {};

export const ReducerFunction = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_USER:
            return {
                ...state, ...action.data
            };
        case CHANGE_ACTIVE_USER_DATA:
            return {
                ...state, ...action.data
            }
        default:
            return state;
    }
};
