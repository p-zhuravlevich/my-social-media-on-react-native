import { createContext, useContext } from 'react';
import {defaultState} from "../context/reducer";

export interface iUserData{
  id: string,
  email: string,
  avatar: string,
  firstName: string,
  lastName: string
}

export interface iUser {
  id: string,
  firstName: string,
  lastName: string,
  avatar: string
}

type iContext = {
  userState: any,
  handleShowUser?: any,
};

export const useAppContext = () => useContext(AppContext); 
const AppContext =  createContext<iContext>({userState: defaultState});
export default AppContext;