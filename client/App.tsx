import React, {FC, useReducer} from 'react';
import Context, {iUser, iUserData} from "./src/context/context";
import Login from "./src/screens/Login";
import * as ACTIONS from "./src/actionCreators/user.action";
import {ReducerFunction, defaultState} from "./src/context/reducer";
import { MainStackNavigator } from './src/navigator/mainNavigator';
import { NavigationContainer } from '@react-navigation/native';


const App: FC = () => {
  const [stateUser, dispatchUserReducer] = useReducer(ReducerFunction, defaultState);

  const handleShowUser = (data) => {
    dispatchUserReducer(ACTIONS.showUser(data));
  };

    return (
      <Context.Provider
            value={{
                userState: stateUser,
                handleShowUser: (data) => handleShowUser(data),
            }}
        >
          <NavigationContainer>
            <MainStackNavigator />
          </NavigationContainer>
      </Context.Provider>
    );
};

export default App;
