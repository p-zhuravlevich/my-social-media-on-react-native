import { UserInterface, User } from "../../src/models/user.model";

declare global{
    namespace Express {
        interface Request {
            user: UserInterface
        }
    }
}
