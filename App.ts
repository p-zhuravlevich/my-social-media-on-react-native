import { Request, Response } from "express";
const express = (require("express"));
import {port, mongoURL} from './src/config/config';
import mongoose from 'mongoose';
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});
const app = express();
const authRouter = require('./src/routers/auth.router');
const userRouter = require('./src/routers/user.router')


const PORT = process.env.PORT || port;


app.use(bodyParser.json());

//ROUTERS 
app.use('/', authRouter);
app.use('/users', userRouter);


(async()=>{
    try{await mongoose.connect(mongoURL,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        }as any)
    app.listen(PORT,()=>console.log(`app running on port ${PORT}`))}
    catch(e){
        console.log(e)
    }
})();





app.get('/', (req: Request, res: Response) => {
    res.setHeader('Content-Type', 'text/html')
    res.end('<h1>This is my final project.</h1>')
});