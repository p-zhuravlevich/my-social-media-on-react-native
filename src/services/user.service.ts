import { Request, Response } from "express";
import {UserInterface, User} from '../models/user.model'
import {IChat, Chat} from '../models/chat.model'

interface CustomRequest<T> extends Request {
  body: T;
}

export const getAllUsers = async () => {
  try {
    const users:any = await User.find({}); 
    console.log(users);    
    return users.map((user: any) => {
      return {
        id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        avatar: user.avatar,
        chats: user.chats,
        invites: user.invites,
        friends: user.friends          
      }
    })  
  } catch(e) {      
    console.log(e)
  }
}

export const sendInviteToFriend = async ({userId, newFriendId}: {userId: string, newFriendId: string}) => {
  try {
    const user: CustomRequest<UserInterface> | any = await User.findById(newFriendId);
    const alreadyInvite = user.invites.includes(userId);
    const alreadyFriend = user.friends.includes(userId);
    if (!alreadyFriend || !alreadyInvite) {
      await User.updateOne({_id: newFriendId}, {invites: [...user.invites, userId]}); 
      return user
    } else {            
      return {
        status: 'error',
        message: 'You are already send invite to friends...'
      }
    }
    
  } catch (e: Error | any) {
    console.log(e)
  }
}


export const getAllInvitations = async (request: CustomRequest<UserInterface> | any, response: Response)=> {
  try {
      const user: CustomRequest<UserInterface> | any = await User.find(request._id);
      if (!user) {
          return response.status(500).json(`can't find user`);
      }
      if (user.invites) {
          return response.status(200).json(user.invites)
      }
      return response.status(200).json(`you don't have invitations`)
  } catch (e : Error | any) {
    console.log('err');

      return response.status(500).json(`${e.message}`)
  }
}


// export const addToFriend = async ({userId, newFriendId}: {userId: string, newFriendId: string}) => {
//   try {
//     const user = await User.findOne({_id: userId}); 
//     const alreadyFriend = user.friends.indexOf(newFriendId);   
//     if(alreadyFriend === -1) {
//       await User.updateOne({_id: userId}, {friends: [...user.friends, newFriendId]});       
//       return {
//         status: 'ok',
//         message: 'New friend was added...'
//       }
//     } else {            
//       return {
//         status: 'error',
//         message: 'You are already friends...'
//       }
//     }     
//   } catch(e) {
//     console.log(e)
//   }
// }