const jwt = require('jsonwebtoken');
import { response } from 'express';
import {accessToken} from '../config/config';
// const User = require('../models/user.model');
import {User} from '../models/user.model'
import {IChat, Chat} from '../models/chat.model'
const bcrypt = require('bcryptjs');

const AuthScheme = require('../validation/auth.scheme');
const LoginScheme = require('../validation/login.scheme');

type AuthBodyT = {
    firstName: string,
    lastName: string,
    email: string,
    password: string
  }

export const register = async(body: AuthBodyT)=>{
  try{
      const {firstName, lastName, email, password} = body;
      const isValid = await AuthScheme.isValid(body);
      if(!isValid){
        return {message: 'Try again... Check your credentials!'}
      }
      const oldUser = await User.findOne({ email });
      if(oldUser){
        return {message: 'User with this email already exist. Try again...'}
      }
      const encryptedPassword = await bcrypt.hash(password, 5);
      const user = await User.create({
        firstName, lastName, email, password: encryptedPassword,
      });
      await user.save();
      console.log(user);
      return user;
    }
    catch(e){
      console.log(e);
      return {message: 'Something went wrong'};
    }
}


export const login = async(body: AuthBodyT)=>{
  try{
    const {email, password} = body;
    const isValid = await LoginScheme.isValid(body);
    if(!isValid){
      return {message: 'Try again... Check your credentials!'}
    }
    const user:any = await User.findOne({email});
    if(!user){
      return {message: 'User is not found.'}
    }
    const truePassword = await bcrypt.compare(password, user.password);
    if(!truePassword){
      return {message: 'Wrong password, try again.'}
    }
    const token = jwt.sign({userId: user.id}, accessToken, {expiresIn: '1h'});
    const refreshToken = jwt.sign({userId: user.id}, accessToken, {expiresIn: '24h'});
    user.accessToken = token;
    user.refreshToken = refreshToken;
    const loginUser = {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,        
      id: user.id,
      friends: user.friends,
      invites: user.invites,
      // chats: user.chats,
      feed: user.feed,
      avatar: user.avatar,
      'accessToken': token, 
      'refreshToken': refreshToken
    };
    console.log(loginUser);
    return loginUser;
  }
  catch(e){
    console.log(e);
    return {message: 'Something went wrong.'};
  }
}