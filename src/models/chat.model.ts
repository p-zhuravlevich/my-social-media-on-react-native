import * as mongoose from "mongoose";
import { UserInterface } from "./user.model";

const Schema = mongoose.Schema;

interface IChat extends mongoose.Document {
  users: Array<UserInterface>
}

const chatSchema = new Schema({
  users: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    autopopulate: true
  }]
},
  { versionKey: false }
)

chatSchema.plugin(require('mongoose-autopopulate'));
const Chat = mongoose.model<IChat>('Chat', chatSchema);

export { IChat, Chat };