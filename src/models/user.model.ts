import * as mongoose from "mongoose";
import {IChat, Chat} from "./chat.model"

const Schema = mongoose.Schema;

interface UserInterface extends mongoose.Document {
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    token: string,
    avatar: string,
    // chats: Array<IChat>,
    friends: Array<UserInterface>,
    feed: Array<string>,
    invites: Array<UserInterface>
}

const UserSchema = new Schema({
    firstName: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 35
        },
    lastName: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 45
        },
    email: { 
        type: String, 
        required: true,
        unique: true
        },
    password: { 
        type: String,
        required: true,
        },
    avatar:{
        type: String,
        default: 'https://snack-web-player.s3.us-west-1.amazonaws.com/v2/42/static/media/react-native-logo.79778b9e.png',
        require: true
    },
    friends:[{
        type: mongoose.Schema.Types.ObjectId,  
        ref: 'User',
        autopopulate: {
            maxDepth: 1
        }
    }],
    // chats: [{
    //     type: String, 
    //     ref: 'Chat',
    //     require: true,
    //     autopopulate: {
    //         maxDepth: 2
    //     }
    // }],
    feed: [{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Feed'
    }],
    invites: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        autopopulate: {
            maxDepth: 1
        }
    }],
    token: { 
        type: String 
    },
}, { versionKey: false });

UserSchema.plugin(require('mongoose-autopopulate'));
const User = mongoose.model<UserInterface>("User", UserSchema);

export {User, UserInterface};