import {Request, Response, NextFunction } from "express";
const jwt = require("jsonwebtoken");
import {accessToken} from '../config/config'; 

module.exports = (req: Request, res: Response, next: NextFunction) => {
    try {    
        if(req.headers.authorization) {       
        const token = req
            .headers
            .authorization
            .split(' ')[1];           
        const decoded = jwt.verify(token, accessToken);         
        next();
        } else {
        res
            .status(401)
            .json({message: 'You have no authorization.'})
        }     
    } catch (e) {
        res
            .status(401)
            .json({message: 'You have no authorization.'})
    }
};