import { Response, Request } from "express";
import { register, login } from "../services/auth.service";


const regUser = async(req: Request, res: Response) => {
    res.send(await register(req.body))
}

const loginUser = async(req: Request, res: Response) => {
    res.send(await login(req.body))
}

module.exports = { regUser, loginUser };