import { Response, Request } from "express";
import { getAllUsers, sendInviteToFriend } from "../services/user.service";


export const showAllUsers = async(req: Request, res: Response) => {
    res.send(await getAllUsers())
}

// export const addFriend = async (req: Request, res: Response) => {    
//     res.send(await addToFriend(req.body))
// }

export const sendInvite = async (req: Request, res: Response) => {    
    res.send(await sendInviteToFriend(req.body))
}
