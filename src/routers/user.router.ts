import * as express from "express";
const UserController = require("../controllers/user.controller");
const UserService = require('../services/user.service')
const userRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});
const authMiddleware = require('../middlewares/auth.middleware')


userRouter.get('/getallusers', urlencodedParser, UserController.showAllUsers);
userRouter.put('/sendInviteFriend', authMiddleware, urlencodedParser, UserController.sendInvite);
userRouter.get('/invites', authMiddleware, urlencodedParser, UserService.getAllInvitations);


module.exports = userRouter;