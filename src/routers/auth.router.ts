import * as express from "express";
const AuthController = require("../controllers/auth.controller");
const authRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});


authRouter.post('/register', urlencodedParser, AuthController.regUser);
authRouter.post('/login', urlencodedParser, AuthController.loginUser);

module.exports = authRouter;